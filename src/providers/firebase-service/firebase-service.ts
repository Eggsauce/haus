import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
//import angular firestore collection so that we can get data from firebase and store in a collection
import { AngularFirestoreCollection, AngularFirestoreDocument, AngularFirestore } from '@angular/fire/firestore';
//import for Get data usage in firebase service provider (information not 100% sure about this line of code)
import 'rxjs/add/operator/map';
import { elementEventFullName } from '@angular/core/src/view';
import { database, User, UserInfo } from 'firebase';
import { observable, identity, Observable } from 'rxjs';
import { Item, List, ListHeader, UrlSerializer,AlertController } from 'ionic-angular';
import { DatePipe } from '@angular/common';
import { dateDataSortValue } from 'ionic-angular/umd/util/datetime-util';
import { AngularFireAuth } from 'angularfire2/auth';
import { ClassField } from '@angular/compiler/src/output/output_ast';
import { useAnimation } from '@angular/core/src/animation/dsl';
import firebase from 'firebase';

/*
  Generated class for the FirebaseServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/


@Injectable()
export class FirebaseServiceProvider {

  usersCollection: AngularFirestoreCollection<any>;
  user: AngularFirestoreDocument<any>;

  journalsCollection : AngularFirestoreCollection<any>;
  journal: AngularFirestoreDocument<any>;

  ledgersDebitCollection : AngularFirestoreCollection<any>;
  ledgerDebit : AngularFirestoreDocument<any>;

  ledgersCreditCollection : AngularFirestoreCollection<any>;
  ledgerCredit : AngularFirestoreDocument<any>;

  totalsCollection : AngularFirestoreCollection<any>;
  total :AngularFirestoreDocument<any>;

  DCdropdownCollection : AngularFirestoreCollection<any>

  UserIdentityDropdown :  AngularFirestoreCollection<any>;


  // to store the id that get from the firebase
  ledgerDebitID : any;
  ledgerCreditID : any;
  journalID : any;
  totalID : any;
  userID : any;
  

  

  

    constructor( 
      private afAuth : AngularFireAuth,
      private afs: AngularFirestore,
      private alertCtrl: AlertController,
      )
    {
      this.usersCollection = this.afs.collection('user');;
      this.journalsCollection = this.afs.collection('journal');
      this.DCdropdownCollection = this.afs.collection('DCdropdown');
      this.ledgersDebitCollection = this.afs.collection('ledgerDebit');
      this.ledgersCreditCollection = this.afs.collection('ledgerCredit');
      this.totalsCollection = this.afs.collection('total');
      this.UserIdentityDropdown = this.afs.collection('userIdentityDropdown');
      
      
    }
    
    
  
   
    createUser(source : any)
    {
      this.userID = source.id;
      this.afs.collection('user').doc(this.userID).set(source); // set the document id same as the firebase authentication uid
       
    }

    /*checkRole(){
      return this.usersCollection.get().subscribe(snapshot =>{
        return snapshot.docs.map(doc =>{
          return doc.data().userIdentity,
          doc.data().email,
          doc.data().password;
        });
      });
    }*/


   

    getUserIdentityDropdown(){
      return this.UserIdentityDropdown.valueChanges(); // get the user role base dropdown from firebase
    }

   
   

    createTotal(source:any){
      this.totalsCollection.add(source).then(source =>{
        this.totalID = source.id;
        this.afs.doc(`total/${source.id}`).update({id : this.totalID});
      });
    }

    createLedgerDebit(source:any){
      
      //add the source with create a id in the field same as doc
      this.ledgersDebitCollection.add(source).then(source =>{
        this.ledgerDebitID = source.id;
        this.afs.doc(`ledgerDebit/${source.id}`).update({id : this.ledgerDebitID});
      });
      
    }

    createLedgerCredit(source:any){
      
      //add the source with create a id in the field same as doc
      this.ledgersCreditCollection.add(source).then(source =>{
        this.ledgerCreditID = source.id;
        this.afs.doc(`ledgerCredit/${source.id}`).update({id : this.ledgerCreditID});
      });
      
    }


    createJournal(source:any){
      
      //add the source with create a id in the field same as doc
      this.journalsCollection.add(source).then(source =>{
        this.journalID = source.id;
        this.afs.doc(`journal/${source.id}`).update({id : this.journalID});
      });
      
    }

    // get the value from firebase
    getdcDropdown(){
      return this.DCdropdownCollection.valueChanges();
      
    }

    getJournalId(id:any){
      return this.journalsCollection.doc(id);
    }

    getJournal(){
      
      return this.afs.collection('journal',ref =>ref.orderBy('date','desc')).valueChanges();
    }

    //is to edit the value by sorting the id
    updateJournal(source: any)
    {
      alert(source);
      
      this.journal = this.afs.doc(`journal/${source.id}`),
      this.journal.update(source);

    }

    // is to delete the value by sorting the id
    deleteJournal(id : any)
    {
      this.journal = this.afs.doc(`journal/${id}`);
      this.journal.delete();
    }


   

    

    

    
   

  

  


}
