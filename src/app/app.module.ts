import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { DisplayAccountPageModule} from '../pages/display-account/display-account.module';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ProfilePage } from '../pages/profile/profile';
import { ProfilePageModule } from '../pages/profile/profile.module';
import { AccountPageModule } from '../pages/account/account.module';
import { EditAccountPageModule } from '../pages/edit-account/edit-account.module';
//import angular module to use firebase and initialize the API config
import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule, AngularFirestore } from 'angularfire2/firestore';
//import firebase service provider
import { FirebaseServiceProvider } from '../providers/firebase-service/firebase-service';
import { RegisterPageModule } from '../pages/register/register.module';
import { LoginPageModule} from '../pages/login/login.module';
import { LedgerPageModule} from '../pages/ledger/ledger.module';
import{ AngularFireAuthModule } from 'angularfire2/auth';
import firebase from 'firebase';
import {FormsModule} from '@angular/forms';
import { SelectSearchableModule } from 'ionic-select-searchable';
import {HttpModule} from '@angular/http';
import { IonicStorageModule } from '@ionic/storage';




 // Initialize Firebase
 const config = {
  apiKey: "AIzaSyBBD_BSXlnHPRMZXK_U1I9eH_zbjMkYafA",
  authDomain: "haus-b766a.firebaseapp.com",
  databaseURL: "https://haus-b766a.firebaseio.com",
  projectId: "haus-b766a",
  storageBucket: "haus-b766a.appspot.com",
  messagingSenderId: "512119140597"
};




@NgModule({
  declarations: [
    MyApp,
    HomePage,
    
 
    
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(config, "HAUS"),
    AngularFirestoreModule,
    AngularFireAuthModule,
    RegisterPageModule,
    LoginPageModule,
    AccountPageModule,
    ProfilePageModule,
    AngularFirestoreModule.enablePersistence(),
    FormsModule,
    SelectSearchableModule,
    HttpModule,
    EditAccountPageModule,
    DisplayAccountPageModule,
    LedgerPageModule,
    IonicStorageModule.forRoot({
      name: '__mydb',
         driverOrder: ['indexeddb', 'sqlite', 'websql']
    })
    
   
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    
   
    
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    FirebaseServiceProvider,
    
  ]
})
export class AppModule {}
