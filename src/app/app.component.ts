import { Component, OnInit } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AngularFirestore} from 'angularfire2/firestore';
import { Subject} from 'rxjs/Subject';
import { Observable} from 'rxjs/Rx';
import { AccountPage } from '../pages/account/account';
import { HomePage } from '../pages/home/home';
import { observable } from 'rxjs';
import { DisplayAccountPage } from '../pages/display-account/display-account';
import { LoginPage } from '../pages/login/login';

@Component({
  templateUrl: 'app.html'
})
export class MyApp implements OnInit{
  rootPage:any = LoginPage;
  searchterm : string;
  startAt = new Subject();
  endAt = new Subject();
  startobs =this. startAt.asObservable();
  endobs = this.endAt.asObservable();

  names;

  constructor(private afs : AngularFirestore,platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }

  ngOnInit(){
    Observable.combineLatest(this.startobs,this.endobs).subscribe((value)=>{
      this.firequery(value[0],value[1]).subscribe((names)=>{
        this.names = names;
      })
    })
  }

  search($event){
    let q = $event.target.value;
    this.startAt.next(q);
    this.endAt.next(q+"\uf8ff");
  }

  firequery(start,end){
    return this.afs.collection('search',ref =>ref.limit(4).orderBy('name').startAt(start).endAt(end)).valueChanges();

  }

  
}

