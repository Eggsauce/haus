import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, DateTime } from 'ionic-angular';
import { Observable} from 'rxjs/Rx';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument} from 'angularfire2/firestore';
import { FirebaseServiceProvider } from '../../providers/firebase-service/firebase-service';
import { DisplayAccountPage } from '../display-account/display-account';
import { timeInterval, debounceTime } from 'rxjs/operators';
import { NUMBER_TYPE } from '@angular/compiler/src/output/output_ast';
import { NumberFormatStyle, getLocaleNumberFormat } from '@angular/common';
import { NumberValueAccessor } from '@angular/forms/src/directives';
import { isNumber } from 'ionic-angular/umd/util/util';



/**
 * Generated class for the AccountPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-account',
  templateUrl: 'account.html',
})


export class AccountPage  {

  

  // a variable that assigned from the AngularFirestoreCollection to read the value 
  debits : Observable<any>;
  credits : Observable<any>;
  
  
  

  debit : string;
  credit : string;
  
  amount : any;
  totalDebit : any;
  totalCredit : any;

  

  date : string = new Date().toLocaleString();
  
  

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public db:AngularFirestore,
    public alertCtrl : AlertController,
    private dbService: FirebaseServiceProvider
    

    ) {
      //assign the AngularFirestoreCollection to the variable (get the value from firebase)
      this.debits = this.dbService.getdcDropdown();
      this.credits = this.dbService.getdcDropdown();

      

      
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AccountPage');
  }

  
 
  submit(){
    /*let totalDebit = this.totalDebit ? parseInt (this.totalDebit) : 0;
    
    totalDebit = totalDebit + amount;*/
    let amount = this.amount ? parseInt (this.amount) : 0;
    try{ 
      
      //add the variable in the new collection call journal in the firebase
      const journal = {
     
       date : this.date,
       debit : this.debit,
       credit : this.credit,
       amount : amount,
      };this.dbService.createJournal(journal);


      if (this.debit == this.debit){

        
        const ledgerDebit ={
        date : this.date,
        debit : this.debit,
        credit : this.credit,
        amount : amount
        
      
      };this.dbService.createLedgerDebit(ledgerDebit),
      this.navCtrl.push(DisplayAccountPage);
    }
     if (this.credit == this.credit){

        
      const ledgerCredit ={
      date : this.date,
      credit : this.credit,
      debit : this.debit,
      amount : amount
      
    
    };this.dbService.createLedgerCredit(ledgerCredit),
    this.navCtrl.push(DisplayAccountPage);
      /*const ledgerDebit ={
       date : this.date,
       debit : this.debit,
       credit : this.credit,
       amount : this.amount,
       
      };this.dbService.createLedgerDebit(ledgerDebit);


      const ledgerCredit ={
       date : this.date,
       debit : this.debit,
       credit : this.credit,
       amount : this.amount,
      };this.dbService.createLedgerCredit(ledgerCredit),
      this.navCtrl.push(DisplayAccountPage);

      
      console.log(ledgerDebit);
      console.log(ledgerCredit);*/
     
      console.log(journal);
     
    }
  }

    catch(e)
    {
        console.error(e);
        console.log('got an error ', e);
        

    }
  }

  

  
    

  

}
