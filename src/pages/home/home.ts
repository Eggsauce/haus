import { Component,OnInit,ViewChild } from '@angular/core';
import { NavController,NavParams,ToastController, App } from 'ionic-angular';
import { LoginPage } from '../login/login';
import { AngularFireAuth } from 'angularfire2/auth';
import { FirebaseServiceProvider } from '../../providers/firebase-service/firebase-service';
import { Subject} from 'rxjs/Subject';
import { Observable} from 'rxjs/Rx';
import { AngularFirestore} from 'angularfire2/firestore';
import { async } from 'rxjs/internal/scheduler/async';
import { SelectSearchableComponent } from 'ionic-select-searchable';
import { AccountPage } from '../account/account';
import { DisplayAccountPage } from '../display-account/display-account';
import { LedgerPage } from '../ledger/ledger';
import { Storage } from '@ionic/storage';
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage implements OnInit{
  searchterm : string;
  startAt = new Subject();
  endAt = new Subject();
  startobs =this. startAt.asObservable();
  endobs = this.endAt.asObservable();
  names;

  role : any;
  roles : string;
  key : string = "role";
  value : string;
  
 
  

  constructor(private afs : AngularFirestore,
    private afAuth : AngularFireAuth,
    private toast : ToastController,
    public navCtrl: NavController, 
    public navParams: NavParams,
    private dbService: FirebaseServiceProvider,
    private appCtrl : App,
    public storage: Storage
 
    ) {

     // this is to get the role in the storage / session, to get this storage is to check the label in the storage and get the value
     // to check the role base, assign the val to a value and check in html 
    this.storage.get(this.key).then((val) => {
      console.log('Role' , val);
      if (val == '1'){
        this.value = val;
      }else if (val == '2'){
        this.value = val;
      };
     })
     
     
  }
  

  ionViewWillLoad(){
    this.afAuth.authState.subscribe(data => {
      if (data && data.email === data.email && data.uid){
      this.toast.create({
        message : `Welcome to APP_NAME, ${data.email}`,
        duration : 3000
      }).present();
      

    }
    else{
      this.toast.create({
        message : `Could not find authentication details`,
        duration : 3000
      }).present();
    }
    
    })
  
  }

  gotoHome(){
    this.navCtrl.push(HomePage);
  }

  gotoLogin(){
    this.navCtrl.push(LoginPage);
  }


  gotoLogout()
  {
    this.afAuth.auth.signOut();
    this.storage.clear();
    this.appCtrl.getRootNav().setRoot(LoginPage);
  }

  gotoLedger(){
    this.navCtrl.push(LedgerPage);
  }

  gotoAccount(){
    this.navCtrl.push(DisplayAccountPage);
  }

  onSearch(event){
    console.log(event.target.value);

  }

  ngOnInit(){
    Observable.combineLatest(this.startobs,this.endobs).subscribe((value)=>{
      this.firequery(value[0],value[1]).subscribe((names)=>{
        this.names = names;
      })
    })
  }

  search($event){
    let q = $event.target.value;
    this.startAt.next(q);
    this.endAt.next(q+"\uf8ff");
    console.log($event.target.value);
  }

  firequery(start,end){
    return this.afs.collection('search',ref =>ref.limit(6).orderBy('name').startAt(start).endAt(end)).valueChanges();

  }



  

 
  

  

}
