import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,AlertController } from 'ionic-angular';
import { Observable} from 'rxjs/Rx';
import { AngularFirestore} from 'angularfire2/firestore';
import { FirebaseServiceProvider } from '../../providers/firebase-service/firebase-service';
/**
 * Generated class for the LedgerPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-ledger',
  templateUrl: 'ledger.html',
})
export class LedgerPage {

  ledgers : Observable<any>;

  constructor(
     public navCtrl: NavController,
     public navParams: NavParams,
     public db:AngularFirestore,
     public alertCtrl : AlertController,
     private dbService: FirebaseServiceProvider,
     ) {
       
       this.ledgers = this.dbService.getdcDropdown();

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LedgerPage');
  }

}
