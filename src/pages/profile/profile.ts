import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AngularFireAuth } from 'angularfire2/auth';
import { FirebaseServiceProvider } from '../../providers/firebase-service/firebase-service';
import { Observable } from 'rxjs';
import { AngularFirestore} from 'angularfire2/firestore';


/**
 * Generated class for the ProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {

  fullname: string;
  email: string;
  phone: string;
  
  

  constructor(private afs: AngularFirestore,private afAuth: AngularFireAuth,
    public navCtrl: NavController, public navParams: NavParams,private dbService: FirebaseServiceProvider) {
      
      
      this.email = afAuth.auth.currentUser.email;
      

      
        
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfilePage');
    
  }

  

 

}
