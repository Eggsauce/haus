import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DisplayAccountPage } from './display-account';

@NgModule({
  declarations: [
    DisplayAccountPage,
  ],
  imports: [
    IonicPageModule.forChild(DisplayAccountPage),
  ],
})
export class DisplayAccountPageModule {}
