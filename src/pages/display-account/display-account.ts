import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { Observable} from 'rxjs/Rx';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument} from 'angularfire2/firestore';
import { FirebaseServiceProvider } from '../../providers/firebase-service/firebase-service';
import { EditAccountPage } from '../edit-account/edit-account';
import { AccountPage } from '../account/account';

/**
 * Generated class for the DisplayAccountPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-display-account',
  templateUrl: 'display-account.html',
})
export class DisplayAccountPage {

  debits : Observable<any>;
  credits : Observable<any>;
  journals : Observable<any>;
  

  debit : string;
  credit : string;
  amount: number;
  

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public db:AngularFirestore,
    public alertCtrl : AlertController,
    private dbService: FirebaseServiceProvider
    ) {

    this.debits = this.dbService.getdcDropdown();
    this.credits = this.dbService.getdcDropdown();
    this.journals = this.dbService.getJournal();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DisplayAccountPage');
  }

  edit(editId){
    this.navCtrl.push(EditAccountPage,
    {
      id: editId
  });
  }


  toAccount(){
    this.navCtrl.push(AccountPage);
  }
}
