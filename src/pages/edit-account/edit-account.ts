import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FirebaseServiceProvider } from '../../providers/firebase-service/firebase-service';
import { Observable} from 'rxjs/Rx';
import { AccountPage } from '../account/account';
import { DisplayAccountPage } from '../display-account/display-account';



/**
 * Generated class for the EditAccountPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-edit-account',
  templateUrl: 'edit-account.html',
})
export class EditAccountPage {


  edit = false;
  debit : string;
  credit : string;
  amount: number;
  date : string = new Date().toLocaleString();
  id : any;

  // a variable that assigned from the AngularFirestoreCollection to read the value 
  debits : Observable<any>;
  credits : Observable<any>;
  journals : Observable<any>;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private dbService: FirebaseServiceProvider
    ) {

      //assign the AngularFirestoreCollection to the variable (get the value from firebase)
      this.debits = this.dbService.getdcDropdown();
      this.credits = this.dbService.getdcDropdown();
      this.journals = this.dbService.getJournal();

      let id = navParams.get('id');
      if(id != null)
      {
        this.id = id;
        this.edit = true;
        this.dbService.getJournalId(id).get().subscribe(doc => {
  
          if(doc.exists)
          {
            // to show the value when want to edit,without this code,the info doesnt show
            this.debit = doc.data().debit;
            this.credit = doc.data().credit;
            this.amount = doc.data().amount;
            
          }
  
          else {
            console.log('Document does not exists!');
          }
        })
      }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EditAccountPage');
  }

  update()
  {
    const journal = {
      date : this.date,
      id : this.id,
      debit : this.debit,
      credit: this.credit,
      amount : this.amount,
    }; this.dbService.updateJournal(journal)
    this.navCtrl.push(DisplayAccountPage);
  }

  remove(id){
    this.dbService.deleteJournal(id);
    alert("Delete successfully");
    this.navCtrl.push(DisplayAccountPage);
  }
  

}
