import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RegisterPage } from './register';
import { SelectSearchableModule } from 'ionic-select-searchable';

@NgModule({
  declarations: [
    RegisterPage,
  ],
  imports: [
    IonicPageModule.forChild(RegisterPage),
    SelectSearchableModule
  ],
})
export class RegisterPageModule {}
