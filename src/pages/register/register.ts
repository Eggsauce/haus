import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { FirebaseServiceProvider } from '../../providers/firebase-service/firebase-service';
import { LoginPage } from '../login/login';
import { AngularFireAuth } from 'angularfire2/auth';
import { User } from 'firebase';
import { Observable } from 'rxjs';

/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})


export class RegisterPage {
  

  fullname: string;
  email: string;
  password: string;
  phone: string;
  identity: number;
  userIdentity : string;

  userIdentitys : Observable<any>;
 
  
 
  constructor(
    private afAuth : AngularFireAuth,
    private alertCtrl: AlertController,
    public navCtrl: NavController, 
    public navParams: NavParams,
    private dbService: FirebaseServiceProvider) {
      
      this.userIdentitys = this.dbService.getUserIdentityDropdown();
   }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');
  }

  alert(message: string) {
    this.alertCtrl.create({
      title: 'Info!',
      subTitle: message,
      buttons: ['OK']
    }).present();
  }

  async register()
  {
    
   
    try{ 
      
      
       const user = this.afAuth.auth.createUserWithEmailAndPassword(this.email,this.password)
      .then(data => {

        console.log('got data ', data);
        this.alert('Successfully Registered!');

        const user = {
        
          fullname: this.fullname,
          email: this.email,
          password: this.password,
          phone: this.phone,
          userIdentity : this.userIdentity,
          id : data.user.uid // add firebase authentication uid's to database 
          
        }; this.dbService.createUser(user);


        this.navCtrl.setRoot(LoginPage);
      })

      

      /*console.log(user);
      alert("Successfully Registered");
      this.navCtrl.setRoot(LoginPage);*/

     
    }

    catch(e)
    {
        console.error(e);
        console.log('got an error ', e);
        this.alert(e.message);

    }
  }
  

}
