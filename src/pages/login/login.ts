import { Component,ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams,ToastController,AlertController } from 'ionic-angular';
import { RegisterPage } from '../register/register';
import { FirebaseServiceProvider } from '../../providers/firebase-service/firebase-service';
import { AngularFireAuth } from 'angularfire2/auth';
import { HomePage} from '../home/home';
import { ProfilePage } from '../profile/profile';
import { SelectSearchableComponent } from 'ionic-select-searchable';
import { Observable, identity } from 'rxjs';
import { docChanges } from 'angularfire2/firestore';
import { ClassField } from '@angular/compiler/src/output/output_ast';
import { UserInfo } from 'firebase';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  
  
  email: string;
  password: string;
  identity : any;
  email1 : any;
  pass : any;
  role : string;
  key : string = "role";
 
 
  
 

  constructor(
    private toast : ToastController,
    private afAuth:AngularFireAuth,
    private alertCtrl: AlertController,
    public navCtrl: NavController, 
    public navParams: NavParams, 
    private dbService: FirebaseServiceProvider,
    public storage: Storage
  
    
    ) {
     
     
      
  }

  

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
    
  }

  checkIdentity(){
    
    
  }

  
  redirect() {
    this.navCtrl.push(RegisterPage);
  }

  alert(message: string) {
    this.alertCtrl.create({
      title: 'Info!',
      subTitle: message,
      buttons: ['OK']
    }).present();
  }

 
  async login()
  {
    
    try{
     

    const user = this.afAuth.auth.signInWithEmailAndPassword(this.email,this.password).
    then( data => {

      


      // call out all the info in user collection for detect purpose
      const users = this.dbService.usersCollection.get().subscribe(snapshot =>{
        snapshot.docs.map(doc =>{

          // assign the info in user collection into a variable so can be use for detect purpose
          this.identity = doc.data().userIdentity; 
          this.email1 = doc.data().email;
          this.pass = doc.data().password;
     
          // use the variable that assign with the value in user collection to check user role base
          if(this.email1 === this.email && this.identity === "Admin"){
            this.alert('Success! You\'re admin');
            this.role = '1';
          
            this.navCtrl.push(HomePage,{
              data : this.storage.set(this.key, this.role) // set the role to the local storage / session (this.key is a label, and this.role is the value)
            });
            
           
          }
          else if(this.email1 === this.email && this.identity === "User")
          {
            this.alert('Success! You\'re user');
            this.role = '2';
           
            this.navCtrl.push(HomePage,{
              data : this.storage.set(this.key, this.role )
            });
           
            
          }
     
          console.log(users)

         
        });
        
      });
      
      console.log(data, user,this.afAuth.auth.currentUser);
     

      //console.log('got some data', this.afAuth.auth.currentUser);
      //this.alert('Success! You\'re logged in');
      
      // user is logged in
    });
    
   

  

    
    }

    catch(e){
      console.error(e);
      console.log('got an error', e);
      this.alert('Sorry,please try again');
    }
  
    

    

  }


  
}
